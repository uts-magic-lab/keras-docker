FROM tensorflow/tensorflow:1.3.0-gpu

RUN pip install --upgrade \
    keras \
    scikit-image \
    theano

ADD assets /
