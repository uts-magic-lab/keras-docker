Docker image to run machine learning jobs. 
Expects to be run in an nvidia docker.

### Usage
```shell
sudo docker build -t magiclab/keras .
sudo docker push magiclab/keras
```
